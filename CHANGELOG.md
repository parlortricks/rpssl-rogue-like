**Change Log**
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---

## [Unreleased]
 * Hand sprites updated
 * UI updated
 * Sprite font added

## [0.1.0] - 2021-10-15

### Added

 * This CHANGELOG file to help
 * Project re-orginization
 * Initial test of code to check winner
 * Hand sprites
 * Inspiration media for ideas

### Fixme
 * Sprite printer needs to be more robust