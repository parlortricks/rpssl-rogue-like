# Rock Paper Scissors Spock Lizard Rogue Like

Can you discover the legendary move of the RPS masters? Traverse the dungeon and battle your way through endless hordes to find the move that will save the world!

## Brief

This game is a Rogue Like idea where the combat mechanic is Rock-Paper-Scissors. Each encounter you battle the enemy to an RPS duel.


- rock=1
- paper=2
- scissors=3
- spock=4
- lizard=5

- rock breaks scissors
- rock crushes lizard
- paper covers rock
- paper disproves spock
- scissors cuts paper
- scissors stabs lizard
- spock smashes scissors
- spock vaporizes rock
- lizard poisons spock
- lizard eats paper


https://stackoverflow.com/a/20214330 - explains how to do the math/code

-- this assumes that "rock", "paper", "scissors", "spock", and "lizard" are assigned any five sequential numbers
-- p1 is player 1's gesture, p2 is player 2's gesture
function winner(p1,p2)
  local diff=(p2-p1)%5
  -- to generalize this to any similar game, replace 5 with the number of moves
  if (diff==0) return nil --tie
  return diff%2+1 --even=p1, odd=p2
end

R,P,S,S,L=1,2,3,4,5 ?

2:39 AM] JWinslow23: example: if p1 chose spock and p2 chose paper, then you could assign it like p1=4 and p2=2. then, (2-4)%5 would be -2%5, or 3. this is odd, so p2 wins!
[12:40 AM] JWinslow23: second example: if p1 chose scissors and p2 chose lizard, then you could assign it like p1=3 and p2=5. then, (5-3)%5 would be 2%5, or 2. this is even (and not 0), so p1 wins!
[12:40 AM] JWinslow23: and of course, if the difference is 0, trivially there is a tie


Emojis for sprites
https://mutant.tech/demo/