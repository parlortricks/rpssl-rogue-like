moves={
    1, --rock
    2, --paper
    3, --scissors
    4, --spock
    5--[[, --lizard
    6  --dynamite]]
}


chars={
    a=224,
    c=225,
    d=226,
    e=227,
    i=228,
    k=229,
    l=230,
    n=231,
    o=232,
    p=233,
    r=234,
    s=235,
    u=236,
    w=237,
    z=238
}

play_again=0
function _init()

end

function _update()

    play_again+=1

    if play_again==30 then play_again = 0 end

end

function _draw()

    --[[spr(0,64,64,4,4)
    --sspr(0,0,32,32,64,64,30,30)
    for i=0, 3 do
        local sx,sy = (i % 4) * 32, (i \ 4) * 32
       
        sspr(sx,sy,32,32,i*32,0,32,32)
    end

    for i=0,10 do
        local offset=240
        spr(i+offset,1+i*8,119)
    end


    sspr(0,32,32,32,0,32)

    sspr(32,32,32,32,32,32)


    sprint("rock",1,70)
    sprint("paper",1,80)
    sprint("scissors",1,90)
    sprint("spock",1,100)
    sprint("lizard",1,110)

    sprint("winner",64,70)

    sprint("loser",64,80)
    sprint("noodles",64,100)


    spr(72,60,35,2,2)
    spr(74,76,35,2,2)
    spr(76,92,35,2,2)
    spr(78,108,35,2,2)
    spr(104,60,52,2,2)
    spr(106,76,52,2,2)]]--


    if play_again ==1 then
        cls(3)
        palt(0,false)
        palt(11,true)
    p1=moves[ceil(rnd(#moves))]
    p2=moves[ceil(rnd(#moves))]

    move_offset=192
    p1_spr=((p1-1)*2)+move_offset
    p2_spr=((p2-1)*2)+move_offset
    --spr(((p1-1)*2)+move_offset,10,10,2,2)

    --spr(((p2-1)*2)+move_offset,30,10,2,2)


    win=winner(p1,p2)

    if win==1 then
        sprint("winner",5,40)
        spr(p1_spr,10,50,2,2)
        sprint("pla",5,70)
        spr(251,30,55,1,1)
        spr(p2_spr,40,50,2,2)
        sprint("cpu",40,70)

    elseif win==2 then
        sprint("loser",5,40)
        spr(p1_spr,40,50,2,2)
        sprint("cpu",5,70)

        spr(251,30,55,1,1)
        spr(p2_spr,10,50,2,2)
        sprint("pla",40,70)

    else
        sprint("draw",5,40)
        spr(p1_spr,10,50,2,2)
        sprint("pla",5,70)
        spr(252,30,55,1,1)
        spr(p2_spr,40,50,2,2)
        sprint("cpu",40,70)
    end

        palt(0,true)
        palt(11,false)  
end


    

end

function sprint(str,x,y)
    local t = {}
    local str,x,y = str,x,y

    for i=1, #str do
        t[i]= sub(str,i,i)
    end

    for i=1,#t do
        spr(chars[tostring(t[i])],x+((i*8)-8),y)
    end
end


-- this assumes that "rock", "paper", "scissors", "spock", and "lizard" 
-- are assigned any five sequential numbers
-- p1 is player 1's gesture, p2 is player 2's gesture
function winner(p1,p2)
    local diff=(p2-p1)%#moves
    -- to generalize this to any similar game, replace 5 with the number of moves
    if (diff==0) return nil --tie
    return diff%2+1 --even=p2, odd=p1
end