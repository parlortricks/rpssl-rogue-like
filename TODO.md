# RPSSL Rogue Like

Creating a rogue like where the combat mechanic is RPSSL

### Todo


### In Progress

- [ ] Create sprites  
  - [ ] Player  
  - [ ] Enemies  
  - [ ] UI  
- [ ] Sprite printer  

### Bugs


### Done ✓

  - [x] Hand gestures  
  - [x] Font  
- [x] Re-organise the project layout  

